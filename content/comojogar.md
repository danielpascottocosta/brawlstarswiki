---
title: "Como jogar"
image: /images/Tutorial.png
draft: false
---

Brawl Stars é um jogo fácil de aprender, mas difícil de dominar! Possui apenas 4 comandos:

### Movimentação

Arraste o botão azul para movimentar seu personagem.

### Ataque

Aperte o botão vermelho para atacar! Caso arraste o botão antes de soltar é possível ainda mirar o ataque.

### Super

Quando carregado, use o botão amarelo para ativar seu Super! Assim como o ataque, é possível mirar arrastando o botão antes de soltar.

### Acessório

Os brawlers ainda contam com um acessório extra! Aperte o botão para acioná-lo, mas cuidado, tem uso limitado de 3 cargas por partida.



