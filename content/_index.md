Esta página é para você que deseja saber tudo sobre Brawl Stars!

### Mas o que é Brawl Stars?

Brawl Stars é um jogo feito para a plataforma mobile, de combate online entre jogadores em diversos modos, como:

- Pique-Gema
- Fute-Brawl
- Nocaute

 Aqui você vai encontrar informações sobre diversos personagens e ainda vai aprender algumas dicas para dominar nas partidas!

