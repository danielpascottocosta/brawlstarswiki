---
title: "Shelly"
image: /images/Shelly.png
draft: false
---

Shelly é dura na queda! Sua espingarda detona a outra equipe com chumbo grosso.

### Atributos

#### Físico

- Saúde: 5320
- Velocidade de movimento: Normal

#### Ataque: Chumbo Grosso

A arma da Shelly dispara chumbinhos em grande amplitude e alcance médio.

 - Dano: 5 x 420
 - Alcance: Longo
 - Velocidade de recarga: Normal

 #### Super: Superbala

 A Superbala da Shelly oblitera coberturas e inimigos. Quem sobrevive é empurrado pra longe.

 - Dano: 9 x 450
 - Alcance: Longo

 #### Dica

 Apesar do longo alcance, Shelly é muito mais efetiva a curtas distâncias, conseguindo carregar seu Super mais rápido a cada acerto!