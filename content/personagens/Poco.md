---
title: "Poco"
image: /images/Poco.png
draft: false
---

Poco é a alma da festa! O som de seu violão é música para aliados e má notícia para inimigos.

### Atributos

#### Físico

- Saúde: 5600
- Velocidade de movimento: Normal

#### Ataque: Dissonância

Poco cria notas desafinadas com seu violão capazes de infligir dano aos inimigos.

 - Dano: 980
 - Alcance: Longo
 - Velocidade de recarga: Normal

 #### Super: Bis!

 Poco pode curar a si mesmo e aos seus aliados com uma melodia digna de bis!

 - Cura: 2940
 - Alcance: Muito longo

 #### Dica

 Apesar de Poco ser um personagem de suporte, não subestime seu ataque! Seu longo alcance e alto dano podem fazer a diferença num combate.