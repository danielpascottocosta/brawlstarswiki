---
title: "Colt"
image: /images/Colt.png
draft: false
---

Colt é um exímio atirador! Ele dispara uma rajada de balas com suas pistolas duplas.

### Atributos

#### Físico

- Saúde: 4000
- Velocidade de movimento: Normal

#### Ataque: Esvaziando o Tambor

Colt dispara seis tiros rápidos de longo alcance com seus revólveres.

 - Dano: 6 x 500
 - Alcance: Longo
 - Velocidade de recarga: Normal

 #### Super: Trem-Bala

 Colt dispara uma rajada de balas de longo alcance que destrói cobertura.

 - Dano: 12 x 450
 - Alcance: Muito longo

 #### Dica

 Além de seu dano devastador, a Super de Colt lhe dá uma vantagem contra oponentes com ataques de pouco alcance ao remover suas coberturas!
